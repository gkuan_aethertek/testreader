//
//  ZMQAPITests.m
//  ZMQAPITests
//
//  Created by Dio on 2020/3/12.
//  Copyright © 2020年 dio. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ZMQAPI.h"

@interface ZMQAPITests : XCTestCase
{
    ZMQPUB *pub;
    ZMQSUB *sub;
    ZMQREQ *req;
    ZMQREP *rep;
}
@end

@implementation ZMQAPITests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getPUBMessage:) name:@"SUB" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getREPMessage:) name:@"REQ" object:nil];
    
    pub = [[ZMQPUB alloc] initWithPort:@"127.0.0.1:3333"];
    sub = [[ZMQSUB alloc] initWithPort:@"127.0.0.1:3333"];
    
    rep = [[ZMQREP alloc] initWithPort:@"127.0.0.1:2222"];
    req = [[ZMQREQ alloc] initWithPort:@"127.0.0.1:2222"];
    
    sub.notificationName = @"SUB";
    rep.notificationName = @"REQ";
}
-(void)testPUBMessage
{
    int index = 0;
    
    while (1) {
        @autoreleasepool {
            [pub sendZMQMessage:[@"PUB TEST" stringByAppendingString:[NSString stringWithFormat:@"%d",index]]];
            [NSThread sleepForTimeInterval:0.1];
            index++;
        }
    }
}
-(void)testREQMessage
{
    int index = 0;
    
    while (1) {
        @autoreleasepool {
            [req sendZMQMessage:[@"REP TEST" stringByAppendingString:[NSString stringWithFormat:@"%d",index]]];
            [NSThread sleepForTimeInterval:0.1];
            index++;
        }
    }
}
- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    [pub Close];
    [sub Close];
    [rep Close];
    [req Close];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

-(void)getPUBMessage:(NSNotification*)notification
{
    NSLog(@"Get PUB msg : %@",[notification object]);
}
-(void)getREPMessage:(NSNotification*)notification
{
    NSLog(@"Get REP msg : %@",[notification object]);
}
@end
