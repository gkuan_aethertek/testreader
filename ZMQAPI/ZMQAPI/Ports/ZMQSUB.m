//
//  ZMQSUB.m
//  ZMQTool
//
//  Created by Louis Pan on 2017/11/27.
//  Copyright © 2017年 SW. All rights reserved.
//

#import "ZMQSUB.h"

@interface ZMQSUB()
{
    NSString* pubChannel;
}
@end

@interface ZMQSUB (Protected)
-(char *)s_recv;
@end

@implementation ZMQSUB
-(instancetype)initWithPort:(NSString*)aZMQPort
{
    self = [super initWithPort:aZMQPort andPortType:ZMQ_SUB];
    if(self)
    {
        pubChannel = @"101";
        self->gcdIdentifier = DISPATCH_QUEUE_PRIORITY_HIGH;
        self.notificationName = ZMQ_POST_NOTIFICATION;
        [self startThread];
    }
    return self;
}
-(void)bind
{
    int rc = zmq_connect (socket, self->zmqPort);
    assert (rc == 0);
    rc = zmq_setsockopt (socket, ZMQ_SUBSCRIBE, [pubChannel UTF8String], (size_t)[pubChannel length]);
    assert (rc == 0);
}
-(void)backgroundWork
{
    char *contents = [self s_recv];
    if(contents)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:self.notificationName object:[NSString stringWithUTF8String:contents]];
        free(contents);
    }
}
@end
