///
//  ZMQREP.m
//  ZMQTool
//
//  Created by Louis Pan on 2017/11/27.
//  Copyright © 2017年 SW. All rights reserved.
//

#import "ZMQREP.h"

@interface ZMQREP (Protected)
-(char *)s_recv;
@end

@implementation ZMQREP
-(instancetype)initWithPort:(NSString*)aZMQPort
{
    self = [super initWithPort:aZMQPort andPortType:ZMQ_REP];
    if(self)
    {
        self->gcdIdentifier = DISPATCH_QUEUE_PRIORITY_DEFAULT;
        self.notificationName = ZMQ_POST_NOTIFICATION;
    }
    [self startThread];
    return self;
}
-(void)bind
{
    int rc = zmq_bind (socket, self->zmqPort);
    assert (rc == 0);
}
-(void)backgroundWork
{
    char *contents = [self s_recv];
    if(contents)
    {
        NSString* recv = [NSString stringWithUTF8String:contents];
        [[NSNotificationCenter defaultCenter] postNotificationName:self.notificationName object:recv];
        [self sendZMQMessage:recv];
        free(contents);
    }
}
@end
