//
//  ZMQDataCenter.m
//  StatistAnalyzsisGraph
//
//  Created by Dio on 2020/3/5.
//  Copyright © 2020年 dio. All rights reserved.
//


#import "ZMQDataCenter.h"


#define ZMQ_IP      @"127.0.0.1"
#define ZMQ_PORT    @"3333"

@interface ZMQDataCenter()
{
    NSMutableDictionary *zmqTable;
    NSLock *queueLock;
}
@end

@implementation ZMQDataCenter

- (id)init
{
    if (self = [super init]){
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(add:) name:[NSString stringWithFormat:@"%@", ZMQ_POST_NOTIFICATION] object:nil];
        
        NSString *zmqAddr = [NSString stringWithFormat:@"%@:%@",ZMQ_IP,ZMQ_PORT];
        self->zmqTable  = [[NSMutableDictionary alloc] init];
        self->queue     = [[NSMutableString alloc] init];
        
        ZMQPUB *pub = [[ZMQPUB alloc] initWithPort:zmqAddr];
        ZMQSUB *sub = [[ZMQSUB alloc] initWithPort:zmqAddr];
        
        [self->zmqTable setObject:pub forKey:@"PUB"];
        [self->zmqTable setObject:sub forKey:@"SUB"];
        
        self->queueLock = [NSLock new];
    }
    return self;
}

+(id)sharedInstance
{
    static ZMQDataCenter *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ZMQDataCenter alloc] init];
    });
    return manager;
}
-(bool)openAllPort
{
    bool isOpen = false;
    
    for(id port in [self->zmqTable allValues])
    {
        isOpen = [port Open];
        if(!isOpen){
            NSLog(@"Open zmq port : %@ error.",port);
            break;
        }
    }
    return isOpen;
}
-(void)closeAllPort
{
    for(id port in [self->zmqTable allValues])
    {
        [port Close];
    }
}
-(void)clearQueue
{
    [self->queueLock lock];
    [self->queue setString:@""];
    [self->queueLock unlock];
}
-(void)add:(NSNotification*)notification
{
    NSLog(@"Get zmq msg : %@",[notification object]);
    [self->queueLock lock];
    [self->queue appendString:[notification object]];
    [self->queueLock unlock];
}
-(NSString *)getQueue
{
    return [self->queue mutableCopy];
}
-(void)debugSendPUB:(NSString *)message
{
    id port = [self->zmqTable objectForKey:@"PUB"];
    [port sendZMQMessage:message];
}
@end
