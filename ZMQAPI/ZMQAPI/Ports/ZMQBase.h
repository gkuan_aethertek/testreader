//
//  ZMQBase.h
//  ZMQTool
//
//  Created by Louis Pan on 2017/11/27.
//  Copyright © 2017年 SW. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "zmq.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

typedef NS_ENUM(int, eZMQType) {
    ZMQType_PUB = ZMQ_PUB,
    ZMQType_SUB = ZMQ_SUB,
    ZMQType_REQ = ZMQ_REQ,
    ZMQType_REP = ZMQ_REP
};

#define ZMQ_POST_NOTIFICATION  @"com.trantest.zmqSendMessage"

@class ArrayQueue;
@interface ZMQBase : NSObject
{
@protected
    const char* zmqPort;
    eZMQType zmqType;
    void *context;
    void *socket;
    ArrayQueue* queue;
    long gcdIdentifier;
    bool bRunThread;
    bool bCompleted;
}
@property (nonatomic) NSString* notificationName;
@property (nonatomic) NSString* index;
-(instancetype)initWithPort:(NSString*)aZMQPort andPortType:(eZMQType)aZMQType;
-(void)sendZMQMessage:(NSString *)string;
-(void)sendMultiZMQMessage:(NSString *)string;
-(bool)Open;
-(void)Close;
-(void)startThread;
-(void)stopThread;
-(bool)isCompleted;
@end
