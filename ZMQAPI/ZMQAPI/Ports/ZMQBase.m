//
//  ZMQBase.m
//  ZMQTool
//
//  Created by Louis Pan on 2017/11/27.
//  Copyright © 2017年 SW. All rights reserved.
//

#import "ZMQBase.h"

@interface ZMQBase()
{
    char* buffer;
    NSUInteger bufferSize;
    NSLock* zmqLock;
    int memsetLen;
    NSLock* zmqSendLock;
}
@end

@implementation ZMQBase
-(instancetype)initWithPort:(NSString*)aZMQPort andPortType:(eZMQType)aZMQType
{
    self = [super init];
    if(self)
    {
        self->zmqPort = [[NSString stringWithFormat:@"tcp://%@", aZMQPort] UTF8String];
        self->zmqType = aZMQType;
        context = zmq_ctx_new ();
        socket = zmq_socket (context, self->zmqType);
        memsetLen = bufferSize = 10*1024*1024;
        buffer = malloc(bufferSize*sizeof(char));
        self->bRunThread = false;
        self->gcdIdentifier = DISPATCH_QUEUE_PRIORITY_DEFAULT;
        bCompleted = true;
        zmqLock = [NSLock new];
        self->zmqSendLock = [NSLock new];
        [self bind];
    }
    return self;
}
-(void)startThread
{
    bRunThread = true;
    dispatch_async(dispatch_get_global_queue(self->gcdIdentifier, 0), ^(){
        while (bRunThread) {
            @autoreleasepool {
                [zmqLock lock];
                [self backgroundWork];
                [zmqLock unlock];
            }
        }
    });
}
-(void)stopThread
{
    bRunThread = false;
}
-(void)bind
{
    @throw [NSException exceptionWithName:[self className]
                                   reason:@"\"bind\" was not implemented"
                                 userInfo:nil];
}
-(char *)s_recv
{
    memset(buffer, '\0', memsetLen*sizeof(char));
    int size = zmq_recv (socket, buffer, bufferSize*sizeof(buffer), 0);
    if (size == -1)
        return NULL;
    memsetLen = size;
    return strndup (buffer, size);
}

-(void)dealloc
{
    zmq_close (self->socket);
    zmq_ctx_destroy (self->context);
    free(buffer);
}
-(void)backgroundWork
{
    @throw [NSException exceptionWithName:[self className]
                                   reason:@"\"backgroundWork\" was not implemented"
                                 userInfo:nil];
}
-(void)sendZMQMessage:(NSString *)string
{
    [self->zmqSendLock lock];
    zmq_send (socket, [string UTF8String], (size_t)[string length], 0);
    [self->zmqSendLock unlock];
}
-(void)sendMultiZMQMessage:(NSString *)string
{
    [self sendZMQMessage:string];
}
-(bool)Open
{
    return socket==0?false:true;
}
-(void)unbind
{
}
-(void)Close
{
    [self stopThread];
    
    zmq_close (self->socket);
    [self unbind];
    zmq_ctx_destroy (self->context);
    if(buffer)
    {
        free(buffer);
        buffer = NULL;
    }
}

-(bool)isCompleted
{
    return bCompleted;
}
@end
