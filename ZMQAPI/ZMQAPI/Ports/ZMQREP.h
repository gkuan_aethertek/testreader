//
//  ZMQREP.h
//  ZMQTool
//
//  Created by Louis Pan on 2017/11/27.
//  Copyright © 2017年 SW. All rights reserved.
//

#import "ZMQBase.h"
//#import "RPCProtocol.h"

@interface ZMQREP : ZMQBase
-(instancetype)init NS_UNAVAILABLE;
-(instancetype)initWithPort:(NSString*)aZMQPort andPortType:(eZMQType)aZMQType NS_UNAVAILABLE;
-(instancetype)initWithPort:(NSString*)aZMQPort;
//@property (weak) id<RPCTestDelegate> repDelegate;
@end
