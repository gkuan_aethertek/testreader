//
//  ZMQREQ.m
//  ZMQTool
//
//  Created by Louis Pan on 2017/11/27.
//  Copyright © 2017年 SW. All rights reserved.
//

#import "ZMQREQ.h"

@interface ZMQREQ (Protected)
-(char *)s_recv;
@end

@implementation ZMQREQ
-(instancetype)initWithPort:(NSString*)aZMQPort
{
    self = [super initWithPort:aZMQPort andPortType:ZMQ_REQ];
    if(self)
    {
        self->gcdIdentifier = DISPATCH_QUEUE_PRIORITY_DEFAULT;
        self.notificationName = ZMQ_POST_NOTIFICATION;
    }
    [self startThread];
    return self;
}
-(void)bind
{
    int rc = zmq_connect (self->socket, self->zmqPort);
    assert (rc == 0);
}

-(void)backgroundWork
{
    char *contents = [self s_recv];
    if(contents)
    {
        NSString* recv = [NSString stringWithUTF8String:contents];
//        NSLog(@"REQ load back : %@",recv);
        free(contents);
    }
}
@end
