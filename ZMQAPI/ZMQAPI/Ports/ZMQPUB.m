//
//  ZMQPUB.m
//  ZMQTool
//
//  Created by Louis Pan on 2017/11/27.
//  Copyright © 2017年 SW. All rights reserved.
//

#import "ZMQPUB.h"

@implementation ZMQPUB
-(instancetype)initWithPort:(NSString*)aZMQPort
{
    self = [super initWithPort:aZMQPort andPortType:ZMQ_PUB];
    if(self)
    {
        self->gcdIdentifier = DISPATCH_QUEUE_PRIORITY_BACKGROUND;
//        [self startThread];
    }
    return self;
}
-(void)bind
{
    int highWaterLevel = 2000;
    int rc = zmq_setsockopt (socket, ZMQ_SNDHWM, &highWaterLevel, sizeof (highWaterLevel));
    assert (rc == 0);
    
    int wait = 1;
    rc = zmq_setsockopt (socket, ZMQ_XPUB_NODROP, &wait, sizeof (wait));
    assert (rc == 0);
    
    rc = zmq_bind (socket, self->zmqPort);
    assert (rc == 0);
}
-(void)backgroundWork
{
//    while ([self->queue count]) {
//        bCompleted = false;
//        NSString* msg = [self->queue dequeue];
//        zmq_send (socket, [msg UTF8String], (size_t)[msg length], 0);
//    }
//    bCompleted = true;
//    usleep(1000);
}
@end
