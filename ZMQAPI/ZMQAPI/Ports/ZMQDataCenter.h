//
//  ZMQDataCenter.h
//  StatistAnalyzsisGraph
//
//  Created by Dio on 2020/3/5.
//  Copyright © 2020年 dio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "zmq.h"
#import "ZMQREQ.h"
#import "ZMQREP.h"
#import "ZMQPUB.h"
#import "ZMQSUB.h"

@interface ZMQDataCenter : NSObject
{
    NSMutableString* queue;
}

+(id)sharedInstance;
-(id)init;
-(bool)openAllPort;
-(void)closeAllPort;
-(void)clearQueue;
-(NSString *)getQueue;
-(void)debugSendPUB:(NSString *)message;

@end

