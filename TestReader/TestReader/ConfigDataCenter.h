//
//  ConfigDataCenter.h
//  StatistAnalyzsisGraph
//
//  Created by Dio on 2020/3/2.
//  Copyright © 2020年 dio. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ConfigDataCenter : NSObject

+(instancetype)sharedInstance;

-(NSString *)getLoadCSVResultName;
-(NSMutableDictionary *)getSystemConfig;
-(NSMutableDictionary *)getGUIConfig;
-(NSMutableDictionary *)getColorConfig;
-(void)saveColorConfig:(NSString *)chart withDictionary:(NSDictionary *)dict;
-(void)saveConfigDictionaryToJson;
-(void)setAndSaveLastLimitConfig:(NSDictionary *)dict;
@end

