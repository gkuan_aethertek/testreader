//
//  ViewController.m
//  TestReader
//
//  Created by Kuan George on 2020/4/22.
//  Copyright © 2020 Kuan George. All rights reserved.
//

#import "ViewController.h"
#import "Module/StringAPI.h"
#import "Module/AnalyzeDataReader.h"
#import "CSVTools/CSVReader.h"
#import "CSVTools/CSVConfiguration.h"
#import "ConfigDataCenter.h"
#import "ZMQDataCenter.h"
@interface ViewController()
{
    NSMutableDictionary *analyzeDataDictionary;
    NSMutableDictionary *systemConfig;
    NSMutableDictionary *advancedParserAllDataDict;
    NSMutableDictionary *limitDictNow;
    
    NSMutableArray *resultCollection;
    
    NSDate* tmpStartData;
    NSString *SUBresult;
    NSLock *testLock;
    NSString *resultDisplay;
}
@property (weak) IBOutlet NSTextField *subZMQView;
@property (weak) IBOutlet NSButton *changeMode;
@end
@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    resultCollection = [NSMutableArray new];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(windowWillClose:) name:NSWindowWillCloseNotification object:[self.view window]];
    // Do any additional setup after loading the view.
    // [self testSingleMessage];
    
    // [self testZMQ];
    
    
    
    // For ZMQ SUB Event
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSUBMessage:) name:SUB_NOTIFICATION object:nil];
    
    self->testLock = [NSLock new];
    self->SUBresult = @"";
    self->resultDisplay = @"";
    self->_nbStartZMQ.toolTip = @"start ZMQ protocol";
    self->_nbAlertPath.toolTip = @"alert someth8ing";
    
    NSTrackingArea *btnArea = [[NSTrackingArea alloc]initWithRect:[self->_nbAlertPath bounds]
                                                          options:NSTrackingMouseEnteredAndExited | NSTrackingActiveAlways
                                                            owner:self
                                                         userInfo:nil];
    [self->_nbAlertPath addTrackingArea:btnArea];
    
}

-(void)mouseExited:(NSEvent *)event
{
    NSLog(@"exit");
}
-(void)mouseEntered:(NSEvent *)event
{
    NSLog(@"entered");
}
-(void)windowWillClose:(NSNotification *)notification
{
     
    id window = [notification object];
    NSString *uid = [window identifier];
    if([uid isEqualToString:@"MainWindow"])
    {
        [NSApp terminate:self];
    }
}
-(IBAction)btnStartZMQClick:(id)sender
{
    [self->_nbStartZMQ setEnabled:false];
    self->_nbStartZMQ.title = @"Processing...";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setStartZMQTrue) name:@"ScriptDone" object:nil];
    NSThread *zmqThread = [[NSThread alloc] initWithTarget:self selector:@selector(testRead) object:nil];
    zmqThread.threadPriority = 1;
    [zmqThread start];
    // [self testRead];
}

-(IBAction)btnChangeModeClick:(id)sender
{
    if(@available(*,macOS 10.14))
    {
        NSAppearance *appearance = NSApp.appearance;
        if(appearance.name == NSAppearanceNameDarkAqua)
        {
            NSAppearance *appAppearance = [NSAppearance appearanceNamed:NSAppearanceNameAqua];
            NSApp.appearance = appAppearance;
        }
        else
        {
            NSAppearance *appAppearance = [NSAppearance appearanceNamed:NSAppearanceNameDarkAqua];
            NSApp.appearance = appAppearance;
        }
    }
}


-(void)setStartZMQTrue
{
    dispatch_async(dispatch_get_main_queue(), ^(){
        [self->_nbStartZMQ setEnabled:true];
        self->_nbStartZMQ.title = @"Start";
    });
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ScriptDone" object:nil];
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

-(void)testSingleMessage
{
    ZMQDataCenter *datacenter = [ZMQDataCenter sharedInstance];
    
    for(int i = 0; i < 10 ; i++)
    {
        NSString *msg = [NSString stringWithFormat:@"message%d",i ];
        [datacenter pubMessage:msg];
        [NSThread sleepForTimeInterval:1];
    }

}

-(void)testZMQ
{
    ZMQDataCenter *datacenter = [ZMQDataCenter sharedInstance];
    //[datacenter testForPUBMessage];
//    NSArray *allkey = [self->advancedParserAllDataDict allKeys];
//    for(NSString *key in allkey)
//    {
//        NSDictionary *testItemDic = [self->advancedParserAllDataDict objectForKey:key];
//        for(NSDictionary *data in testItemDic)
//        {
//
//            // [NSThread sleepForTimeInterval:1];
//        }
//    }b

    [datacenter pubSingleMessage:self->advancedParserAllDataDict andInterval:1];

}

-(void)testRead
{
    
    self->analyzeDataDictionary      = [[NSMutableDictionary alloc] init];
    self->advancedParserAllDataDict = [[NSMutableDictionary alloc] init];
    self->systemConfig              = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *defaultConfig = [[[ConfigDataCenter sharedInstance] getGUIConfig] objectForKey:@"Last_LimitData"];
    if(defaultConfig != nil || defaultConfig == 0)
    {
        self->limitDictNow = defaultConfig;
    }
    else
        self->limitDictNow = [[NSMutableDictionary alloc] init];
    
    // NSString *Path = @"/Users/george/Desktop/LCRTest_TEST2.csv";// /Users/george/Desktop/ProjectFile/WR/CSV_Test.csv
    
    
    // NSString *Path = @"/Users/george/Desktop/ProjectFile/WR/CSV_Test.csv";
    
    NSString *Path = _ntPath.stringValue;
    self->systemConfig = [[ConfigDataCenter sharedInstance] getSystemConfig];
     
     // Load from Database or CSV file
     if([Path length] > 0){
         [self->systemConfig setObject:Path forKey:@"LastLoadCSVFile"];
         [[ConfigDataCenter sharedInstance] saveConfigDictionaryToJson];
         self->analyzeDataDictionary = [AnalyzeDataReader getTestResultDataWithConfigDictionary:self->systemConfig];
         
         for(int i = 0 ; i <[self getTestItems].count ; i++ )
         {
             NSString *itemsName = [[self getTestItems] objectAtIndex:i];
             [self->advancedParserAllDataDict setObject:[[self getAdvancedParserAllData] objectForKey:itemsName] forKey:itemsName];
             [self setLimitNowFromCSV];
         }
         [self transFormat];
         
         NSLog(@"read done");
         [[NSNotificationCenter defaultCenter] postNotificationName:@"ScriptDone" object:nil userInfo:nil];
     }
    
    else
    {
        NSLog(@"read error");
        //[self->_nbStartZMQ setEnabled:true];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ScriptDone" object:nil userInfo:nil];
    }
}
-(void)getSUBMessage:(NSNotification *)notification
{
    [self->testLock lock];

    self->SUBresult = [notification object];
    
    [self->testLock unlock];
}
-(void)transFormat
{
    // NSMutableDictionary *dataCollection = [[NSMutableDictionary alloc]init];
    NSArray *firstItemArr = [self getFirstObject];
    [ZMQDataCenter sharedInstance];
    [NSThread sleepForTimeInterval:1];
    
    for(int i = 0 ; i < [self getFirstObjectCount]; i++)
    {
        NSMutableDictionary *dataCollection = [[NSMutableDictionary alloc]init];
        NSDictionary *dic = [firstItemArr objectAtIndex:i];
        [dataCollection setObject:[dic objectForKey:@"Config"] forKey:@"Config"];
        [dataCollection setObject:[dic objectForKey:@"StartTime"] forKey:@"StartTime"];
        // [dataCollection setObject:[dic objectForKey:@"SN"] forKey:@"SN"];
        [dataCollection setObject:[dic objectForKey:@"EndTime"] forKey:@"EndTime"];
        [dataCollection setObject:[self->advancedParserAllDataDict allKeys] forKey:@"TestItems"];
        NSMutableDictionary *testResult = [self getTestResult:i];
        [dataCollection setObject:testResult forKey:@"TestResult"];

        // Added UUID
        
        NSString *zmqID = [self uniqueIdForObject];
        [dataCollection setObject:zmqID forKey:@"UUID"];
        
        // ZMQDataCenter *datacenter = [ZMQDataCenter sharedInstance];
        NSString *dataStr = [StringAPI NSDictionaryToNSString:dataCollection];

        tmpStartData = [NSDate date];
        
//        NSLog(@"%@ [Start-1] %f",zmqID,[[NSDate date] timeIntervalSinceDate:self->tmpStartData]);
        
        [[ZMQDataCenter sharedInstance] pubMessage:[dataStr mutableCopy]];
        
//        NSLog(@"%@ [Start-2] %f",zmqID,[[NSDate date] timeIntervalSinceDate:self->tmpStartData]);
        
        __block NSString *str;
        __block NSString *result;
        
        while ([self->SUBresult isEqualToString:@""]) {
//            NSLog(@"%@ [Waitting] %.2f",zmqID,[[NSDate date] timeIntervalSinceDate:self->tmpStartData]);
            if([self->SUBresult containsString:zmqID] == false)
                continue;
            else
                break;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(){
            str = self.subZMQView.stringValue;
        });
        
        NSData *data = [self->SUBresult dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        result = [NSString stringWithFormat:@"Result : %@\tCost Time : %.5f\tNote :%@",[json objectForKey:@"Status"],[[NSDate date] timeIntervalSinceDate:self->tmpStartData],[json objectForKey:@"Note"]];
        
        if([json objectForKey:@"UUID"] && [[json objectForKey:@"UUID"] length] > 0)
        {
            if([[json objectForKey:@"UUID"] isEqualToString:zmqID]){
                dispatch_async(dispatch_get_main_queue(), ^(){
                    [self.subZMQView setStringValue:[NSString stringWithFormat:@"%@\n%@",result,str]];
                });
                self->resultDisplay = [NSString stringWithFormat:@"%@\n",result];
                NSLog(@"==>%@",self->resultDisplay );
                self->SUBresult = @"";
            }
        }
        else
            NSLog(@"%@ [ERROR] %f =============================================================",zmqID,[[NSDate date] timeIntervalSinceDate:self->tmpStartData]);
        
        sleep(1);
//        NSLog(@"%@ [End] %f",zmqID,[[NSDate date] timeIntervalSinceDate:self->tmpStartData]);
    }
}
- (NSString *)uniqueIdForObject
{
    CFUUIDRef uniqueString = CFUUIDCreate(NULL);
    CFStringRef isString = CFUUIDCreateString(NULL, uniqueString);
    CFRelease(uniqueString);
    return [NSString stringWithFormat:@"%@",isString];
}

-(NSMutableDictionary *)getTestResult:(int)index
{
    NSArray *allkeys = [self->advancedParserAllDataDict allKeys];
    NSMutableDictionary *testResult = [[NSMutableDictionary alloc]init];
    for(NSString *key in allkeys)
    {
        NSArray *itemArr = [self->advancedParserAllDataDict objectForKey:key];
        NSMutableDictionary *itemDic = [itemArr objectAtIndex:index];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        
        NSString *usl = [NSString stringWithFormat:@"%f",[[itemDic objectForKey:@"Upper Limit----->"] doubleValue]];
        [dic setObject:usl forKey:@"Upper"];
        
        NSString *lsl = [NSString stringWithFormat:@"%f",[[itemDic objectForKey:@"Lower Limit----->"]  doubleValue]];
        [dic setObject:lsl forKey:@"Lower"];
        NSNumber *iResult =[itemDic objectForKey:@"Result"] ;
        NSString *result = [NSString stringWithFormat:@"%f",[iResult doubleValue]];
        [dic setObject:result forKey:@"Result"];
        [testResult setObject:dic forKey:key];
    }
    return testResult;
}

-(NSArray *)getTestItems
{
    
    if(  self->analyzeDataDictionary == nil || [self->analyzeDataDictionary count] == 0 )
        return nil;
    
    
    NSArray *result = [[[self->analyzeDataDictionary objectForKey:@"TestItems"] allKeys] mutableCopy] ;
    
    return result;
}
-(NSArray *)getFirstObject
{
    NSArray *allkeys = [self->advancedParserAllDataDict allKeys];
    for(NSString *key in allkeys)
    {
        return [self->advancedParserAllDataDict objectForKey:key];
    }
    return [[NSArray alloc]init];;
}
-(NSInteger)getFirstObjectCount
{
    NSArray *allkeys = [self->advancedParserAllDataDict allKeys];
    for(NSString *key in allkeys)
    {
        return [[self->advancedParserAllDataDict objectForKey:key]count];
    }
    return 0;
}

-(NSDictionary *)getAdvancedParserAllData
{
    NSMutableDictionary *collectinResultDictionary  = [[NSMutableDictionary alloc] init];
    
    NSDictionary *testItems = [self->analyzeDataDictionary objectForKey:@"TestItems"];
    NSDictionary *CSVConfig = [self->analyzeDataDictionary objectForKey:@"CSVConfig"];
    
    for(NSString *key in [testItems allKeys])
    {
        NSMutableArray *collectArray = [[NSMutableArray alloc] init];
        NSMutableDictionary *collectResult = [[NSMutableDictionary alloc] init];
        for(NSString *csvKey in [CSVConfig allKeys])
        {
            NSArray *csvContent = [[CSVConfig objectForKey:csvKey] objectForKey:@"Content"];
            [collectResult setObject:csvContent forKey:csvKey];
        }
        for(int i = 0; i < [self getArrayMaxCount:collectResult] ; i++)
        {
            bool passfailResult = true;
            NSMutableDictionary *collectDict = [NSMutableDictionary new];
            for(NSString *subKey in collectResult.allKeys)
            {
                NSArray *subarray = [collectResult objectForKey:subKey];
                if([subarray count] <[self getArrayMaxCount:collectResult])
                    continue;

                [collectDict setObject:[subarray objectAtIndex:i] forKey:subKey];
                
                for(id subTestItems in [testItems objectForKey:key])
                {
                    if([subTestItems isEqualToString:@"Content"])
                    {
                        NSArray *subItemArray = [[testItems objectForKey:key] objectForKey:@"Content"];
                        [collectDict setObject:[subItemArray objectAtIndex:i] forKey:@"Result"];
                    }
                    else{
                        NSString *otherInfoKey = subTestItems;
                        id keyValue = [[testItems objectForKey:key] objectForKey:otherInfoKey];
                        [collectDict setObject:keyValue forKey:otherInfoKey];
                    }
                }
            }
            for(NSString *key in collectDict)
            {
                if([[key uppercaseString] containsString:@"FAIL"] && [[key uppercaseString] containsString:@"PASS"])
                {
                    passfailResult = [[collectDict objectForKey:key] containsString:@"FAIL"] ? false : true;
                    break;
                }
            }
            passfailResult == true ? [collectArray addObject:collectDict] : passfailResult;
        }
        [collectinResultDictionary setObject:[collectArray mutableCopy] forKey:key];
    }
    
    return collectinResultDictionary;
}
-(NSInteger)getArrayMaxCount:(NSMutableDictionary *)dict;
{
    NSMutableArray *totalcount = [[NSMutableArray alloc] init];
    for (id subItems in dict.allKeys) {
        NSArray *subArray = [dict objectForKey:subItems];
        [totalcount addObject:[NSNumber numberWithInteger:subArray.count]];
    }
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
    [totalcount sortUsingDescriptors:[NSArray arrayWithObjects:sort, nil]];
    
    NSInteger maxCount = [[totalcount firstObject] integerValue];
    return maxCount;
}
-(void)setLimitNowFromCSV
{
    NSArray *allKeys = [advancedParserAllDataDict allKeys];
    
    NSMutableDictionary *last = [[NSMutableDictionary alloc] init];
    
    for (int i = 0 ; i< allKeys.count; i++) {
        
        NSString *testItems = [allKeys objectAtIndex:i];
        NSDictionary *subDict = [[advancedParserAllDataDict objectForKey:testItems] firstObject];
        if(subDict == nil || [subDict count] == 0 )
            break;
        
        NSString *limitUpperKey = [NSString stringWithFormat:@"%@",[subDict objectForKey:@"Upper Limit----->"]];
        NSString *limitLowerKey = [NSString stringWithFormat:@"%@",[subDict objectForKey:@"Lower Limit----->"]];
        
        [last setObject:limitUpperKey forKey:[testItems stringByAppendingString:@"-Upper"]];
        [last setObject:limitLowerKey forKey:[testItems stringByAppendingString:@"-Lower"]];
    }
    
    if(last != nil && [last count] != 0){
        self->limitDictNow = last;
        [[[ConfigDataCenter sharedInstance] getGUIConfig] setObject:last forKey:@"Last_LimitData"];
        [[ConfigDataCenter sharedInstance] saveConfigDictionaryToJson];
    }
}
@end
