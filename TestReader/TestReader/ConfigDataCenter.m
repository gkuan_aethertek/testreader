//
//  ConfigDataCenter.m
//  StatistAnalyzsisGraph
//
//  Created by Dio on 2020/3/2.
//  Copyright © 2020年 dio. All rights reserved.
//

#import "ConfigDataCenter.h"
#import "CSVReader.h"

@interface ConfigDataCenter ()
{
    // NSString *jsonPath;
    NSMutableDictionary *dictionary;
    NSLock *lock;
    
    NSMutableArray *pathArr;
}
@end

@implementation ConfigDataCenter

-(instancetype)init
{
    self = [super init];
    
    if(self)
    {
        pathArr = [[NSMutableArray alloc]init];
        dictionary = [[NSMutableDictionary alloc] init];
        lock = [NSLock new];
        [self initConfigDataCenter];
    }
    
    return self;
}
+(instancetype)sharedInstance
{
    // 1
    static ConfigDataCenter *_sharedInstance = nil;
    
    // 2
    static dispatch_once_t oncePredicate;
    
    // 3
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[ConfigDataCenter alloc] init];
    });
    return _sharedInstance;
}

-(void)initConfigDataCenter
{
    NSString *systemPath = [[[NSBundle mainBundle] resourcePath]   stringByAppendingString:@"/systemConfig.json"];
    [self addToPathArray:systemPath andKey:@"systemConfig"];
    NSString *GUIPath = [[[NSBundle mainBundle] resourcePath]  stringByAppendingString:@"/GUIConfig.json"];
    [self addToPathArray:GUIPath andKey:@"GUIConfig"];
    NSString *colorPath = [[[NSBundle mainBundle] resourcePath]  stringByAppendingString:@"/ColorConfig.json"];
    [self addToPathArray:colorPath andKey:@"ColorConfig"];
    
//    jsonPath = [[[[NSBundle mainBundle] resourcePath] stringByDeletingLastPathComponent]  stringByAppendingString:@"/Resources/Config.json"];
//    if ([[NSFileManager defaultManager] fileExistsAtPath:jsonPath])
//        dictionary = [[CSVReader JSONFromPath:jsonPath] mutableCopy];

    // Set GUI config
    [self loadConfigFromPath];
    
//    if(![dictionary objectForKey:@"systemConfig"]){
//        [dictionary setObject:[defaultConfig objectForKey:@"systemConfig"] forKey:@"systemConfig"];}
//
//    if(![dictionary objectForKey:@"GUIConfig"]){
//        [dictionary setObject:[defaultConfig objectForKey:@"GUIConfig"] forKey:@"GUIConfig"];}
//
//    if(![dictionary objectForKey:@"ColorConfig"]){
//        [dictionary setObject:[defaultConfig objectForKey:@"ColorConfig"] forKey:@"ColorConfig"];}
    
    [self saveConfigDictionaryToJson];
}
-(NSString *)getLoadCSVResultName
{
    NSString *path = [[dictionary objectForKey:@"systemConfig"] objectForKey:@"LastLoadCSVFile"] ? [[[dictionary objectForKey:@"systemConfig"] objectForKey:@"LastLoadCSVFile"] lastPathComponent] : @"No CSV result loaded";
    
    return path;
}

-(NSMutableDictionary *)getSystemConfig
{
    return [dictionary objectForKey:@"systemConfig"];
}
-(NSMutableDictionary *)getGUIConfig
{
    return [dictionary objectForKey:@"GUIConfig"];
}
-(NSMutableDictionary *)getColorConfig
{
    return [dictionary objectForKey:@"ColorConfig"];
}

-(void)setAndSaveLastLimitConfig:(NSDictionary *)dict
{
    NSMutableDictionary *temp = [dictionary objectForKey:@"GUIConfig"];
    [temp setObject:dict forKey:@"Last_LimitData"];
    [self saveConfigDictionaryToJson];
}

-(void)saveColorConfig:(NSString *)chart withDictionary:(NSDictionary *)dict
{
    [[dictionary objectForKey:@"ColorConfig"] setObject:dict forKey:chart];
    [self saveConfigDictionaryToJson];
}

-(void)saveConfigDictionaryToJson
{
    NSError *err;
    
    if(dictionary == nil)
        return;
    
    for(NSDictionary *configDic in pathArr)
    {
        
        [lock lock];
        NSString *path = [configDic objectForKey:@"Path"];
        NSString *name = [configDic objectForKey:@"Name"];
        NSDictionary *dic = [dictionary objectForKey:name];
        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                           options:NSJSONWritingPrettyPrinted error:&err];
        
        
        if(jsonData != nil && [path length] > 0)
        {
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [jsonString writeToFile:path
                         atomically:NO
                           encoding:NSStringEncodingConversionAllowLossy
                              error:&err];
        }
        else
            NSLog(@"Save config failed!");
       [lock unlock];
        
    }
}

#pragma mark - new

-(void)addToPathArray:(NSString *)path andKey:(NSString *)pathKey
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:path forKey:@"Path"];
    [dic setObject:pathKey forKey:@"Name"];
    [pathArr addObject:dic];
}

-(void)loadConfigFromPath
{
    
    NSMutableDictionary *defaultConfig = [@{@"systemConfig":
                                                [@{
                                                    @"LastLoadCSVFile":@"",
                                                    @"TitleItemsName" : @"SerialNumber",
                                                    @"TitleColumLength":@(4),
                                                    @"ItemColumTargets":@[@(12),@(13),@(14),@(15)],
                                                    @"LowerLimit":@"Lower Limit----->",
                                                    @"UpperLimit":@"Upper Limit----->",
                                                    @"MeasurementUnit":@"Measurement Unit----->"
                                                    }mutableCopy],
                                            @"GUIConfig":
                                                [@{
                                                    }mutableCopy],
                                            @"ColorConfig":
                                            [@{}mutableCopy]
                                            }mutableCopy];
    
    for(NSDictionary *configDic in pathArr)
    {
        NSString *path = [configDic objectForKey:@"Path"];
        NSString *name = [configDic objectForKey:@"Name"];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        if ([[NSFileManager defaultManager] fileExistsAtPath:path])
        {
            dic = [[CSVReader JSONFromPath:path] mutableCopy];
            if([dic count] > 0)
                [dictionary setObject:dic forKey:name];
            else
                [dictionary setObject:[defaultConfig objectForKey:name] forKey:name];
        }
        else
            [dictionary setObject:[defaultConfig objectForKey:name] forKey:name];
    }
    
    
}


@end
