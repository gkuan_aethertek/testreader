//
//  AppDelegate.h
//  TestReader
//
//  Created by Kuan George on 2020/4/22.
//  Copyright © 2020 Kuan George. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@end

