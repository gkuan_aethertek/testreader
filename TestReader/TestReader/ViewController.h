//
//  ViewController.h
//  TestReader
//
//  Created by Kuan George on 2020/4/22.
//  Copyright © 2020 Kuan George. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController

@property IBOutlet NSTextField* ntPath;
@property IBOutlet NSButton* nbStartZMQ;
@property IBOutlet NSButton* nbAlertPath;
-(IBAction)btnStartZMQClick:(id)sender;


@end

