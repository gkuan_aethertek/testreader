//
//  StringAPI.m
//  StatistAnalyzsisGraph
//
//  Created by dio on 2020/2/25.
//  Copyright © 2020 dio. All rights reserved.
//

#import "StringAPI.h"

@implementation StringAPI

+(NSString *)regEXResultValue:(NSString *)readString REGEX_PATTEN:(NSString *)pattern REGEX_INDEX:(NSUInteger)index
{
    if([readString length] == 0 || readString == nil)
        return @"Regex target fail";
    
    pattern = [pattern stringByReplacingOccurrencesOfString:@"\\n" withString:@"\\\n"];
    pattern = [pattern stringByReplacingOccurrencesOfString:@"\\t" withString:@"\\\t"];
    pattern = [pattern stringByReplacingOccurrencesOfString:@"\\r" withString:@"\\\r"];
    
    NSMutableArray *resultGroup = [[NSMutableArray alloc]init];
    NSError* error = nil;
    NSRegularExpression *nameExpression = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive | NSRegularExpressionAnchorsMatchLines error:&error];
    if(nameExpression == nil || error != nil)
        return [error description];
    
    NSArray *matches = [nameExpression matchesInString:readString
                                               options:NSMatchingReportProgress
                                                 range:NSMakeRange(0, [readString length])];
    for (NSTextCheckingResult *match in matches) {
        NSInteger num = match.numberOfRanges;
        
        for(int i = 0; i < num; i++)
        {
            NSRange matchRange = [match rangeAtIndex:i];
            NSString *matchString = [readString substringWithRange:matchRange];
            
            [resultGroup addObject:matchString];
        }
    }
    if(resultGroup.count == 0 || index >= resultGroup.count)
        return @"Regex target fail";
    
//    NSLog(@"Get : %@",resultGroup);
    return [resultGroup objectAtIndex:index];
}
+(NSArray *)parserStringWithOutExpectSymbol:(NSArray *)xAxis withSymbol:(NSString *)symbol
{
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    for (NSString *string in xAxis) {
        NSString *result = [NSString stringWithFormat:@"%.2f",[[[string componentsSeparatedByString:symbol] firstObject] doubleValue]];
        [temp addObject:result];
    }
    return temp;
}
+(bool) isNumeric:(NSString*) checkText{
    return [[NSScanner scannerWithString:checkText] scanDouble:NULL];
}
+ (NSString *)NSDictionaryToNSString:(NSDictionary *)dictionary {
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:0];
    NSString *dataStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    if([dataStr length] == 0 || dataStr == nil)
        return @"";
    
    return dataStr;
}
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json load failed：%@",err);
        return nil;
    }
    return dic;
}
+ (NSString *)dateToString:(NSDate *)date withFormat:(NSString *)format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSString *strDate = [dateFormatter stringFromDate:date];
    
    return strDate;
}
+ (NSDate *)stringToDate:(NSString *)str withFormat:(NSString *)format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSDate *date = [dateFormatter dateFromString:str];
    return date;
}
@end
