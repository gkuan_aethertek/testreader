//
//  ZMQDataCenter.m
//  StatistAnalyzsisGraph
//
//  Created by Dio on 2020/3/19.
//  Copyright © 2020年 dio. All rights reserved.
//

#import "ZMQDataCenter.h"
#import "StringAPI.h"

@interface ZMQDataCenter()
{
    NSString *ip;
    NSString *port;
    bool isPortReceive;
    ZMQSUB *sub;
    
    ZMQPUB *pub;
}
@end

@implementation ZMQDataCenter

- (instancetype)init {
    self = [super init];
    if (self) {
        self->ip = IP_ADDR;
        self->port = PORT;
        self->isPortReceive = false;
        [self iniPortConfig];
        
        
        NSString *portConfig = [NSString stringWithFormat:@"%@:%@",self->ip,self->port];
        pub = [[ZMQPUB alloc] initWithPort:portConfig];
//        // Debug used
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
//            [self testForPUBMessage];
//        });
    }
    return self;
}

+(id)sharedInstance
{
    static ZMQDataCenter *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ZMQDataCenter alloc] init];
    });
    return manager;
}
-(void)iniPortConfig
{
    NSString *portConfig = [NSString stringWithFormat:@"%@:%@",self->ip,@"8044"];
    sub = [[ZMQSUB alloc] initWithPort:portConfig];
    sub.notificationName = SUB_NOTIFICATION;
}
-(bool)openZMQSubPort
{
    return [sub Open];
}
-(void)testForPUBMessage
{
    NSDictionary *temp = @{@"123" : @"456"};
    
    NSString *portConfig = [NSString stringWithFormat:@"%@:%@",self->ip,self->port];
    ZMQPUB *pub = [[ZMQPUB alloc] initWithPort:portConfig];

    while (1) {
        @autoreleasepool {
            NSString *dataStr = [StringAPI NSDictionaryToNSString:temp];
            [pub sendZMQMessage:dataStr];
            [NSThread sleepForTimeInterval:0.01];
        }
    }
}

-(void)pubSingleMessage:(NSDictionary *)dic andInterval:(NSInteger)interval
{
    @autoreleasepool {
        
        NSString *portConfig = [NSString stringWithFormat:@"%@:%@",self->ip,self->port];
        ZMQPUB *pub = [[ZMQPUB alloc] initWithPort:portConfig];

        NSArray *allkey = [dic allKeys];
        for(NSString *key in allkey)
        {
            NSDictionary *testItemDic = [dic objectForKey:key];
            for(NSDictionary *data in testItemDic)
            {
                NSString *dataStr = [StringAPI NSDictionaryToNSString:data];
                [pub sendZMQMessage:dataStr];
                [NSThread sleepForTimeInterval:interval];
            }
        }


    }
}

-(void)pubMessage:(NSString *)message
{
    
    [pub sendZMQMessage:message];
//    @autoreleasepool {
//
//        // NSString *dataStr = [StringAPI NSDictionaryToNSString:message];
//
//    }
}
@end
