//
//  ZMQDataCenter.h
//  StatistAnalyzsisGraph
//
//  Created by Dio on 2020/3/19.
//  Copyright © 2020年 dio. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ZMQAPI/ZMQAPI.h"

#define IP_ADDR @"127.0.0.1"
#define PORT    @"3333"
#define SUB_NOTIFICATION    @"tt.com.zmqsub.notification"

@interface ZMQDataCenter : NSObject

+(id)sharedInstance;

-(bool)openZMQSubPort;
-(void)testForPUBMessage;
-(void)pubSingleMessage:(NSDictionary *)dic andInterval:(NSInteger)interval;
-(void)pubMessage:(NSString *)message;
@end
