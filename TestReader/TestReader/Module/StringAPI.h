//
//  StringAPI.h
//  StatistAnalyzsisGraph
//
//  Created by dio on 2020/2/25.
//  Copyright © 2020 dio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringAPI : NSObject

+(NSString *)regEXResultValue:(NSString *)readString REGEX_PATTEN:(NSString *)pattern REGEX_INDEX:(NSUInteger)index;
+(NSArray *)parserStringWithOutExpectSymbol:(NSArray *)xAxis withSymbol:(NSString *)symbol;
+(bool) isNumeric:(NSString*) checkText;
+ (NSString *)NSDictionaryToNSString:(NSDictionary *)dictionary;
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

+ (NSString *)dateToString:(NSDate *)date withFormat:(NSString *)format;
+ (NSDate *)stringToDate:(NSString *)str withFormat:(NSString *)format;

@end
