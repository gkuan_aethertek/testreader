//
//  AnalyzeDataReader.h
//  StatistAnalyzsisGraph
//
//  Created by Dio on 2020/2/11.
//  Copyright © 2020年 dio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSVConfiguration.h"
#import "CSVReader.h"

@interface AnalyzeDataReader : NSObject

+(NSMutableDictionary *)getTestResultDataWithConfigDictionary:(NSDictionary *)configDict;

@end
