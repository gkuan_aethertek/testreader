//
//  AnalyzeDataReader.m
//  StatistAnalyzsisGraph
//
//  Created by Dio on 2020/2/11.
//  Copyright © 2020年 dio. All rights reserved.
//

#import "AnalyzeDataReader.h"

@implementation AnalyzeDataReader

+(NSInteger)getLastTestItemListFirstNum:(CSVReader *)reader withTitleKey:(NSString *)keyString
{
    NSMutableArray *recordArray = [[NSMutableArray alloc] init];
    NSInteger index = 1;
    NSError* error;
    while(!reader.isAtEnd) {
        NSArray *line = [reader readLineWithError:&error];
        NSString *firstItem = [NSString stringWithFormat:@"%@",line.firstObject];
        if([firstItem isEqualToString:keyString])
            [recordArray addObject:@(index)];
        index++;
    }
//    NSLog(@"Get all title index = %@",recordArray);
    return [[recordArray lastObject] integerValue];
}

+(NSMutableDictionary *)getTestResultDataWithConfigDictionary:(NSDictionary *)configDict
{
    NSString *path = [configDict objectForKey:@"LastLoadCSVFile"];
    
    NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc] init];
    [dataDictionary setObject:[@{} mutableCopy] forKey:@"TestItems"];
    [dataDictionary setObject:[@{} mutableCopy] forKey:@"CSVConfig"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath: path]) {
        
        CSVConfiguration* config = [[CSVConfiguration alloc] init];
        config.firstRowAsHeader = YES;
        NSURL *url =[[NSURL alloc] initFileURLWithPath:path];
        
        NSError* error2 = nil;
        NSData *tmpdata = [NSData dataWithContentsOfURL:url options:NSDataReadingUncached error:&error2];
        
        // NSData* data = [NSData dataWithContentsOfURL:yourURL options:NSDataReadingUncached error:&error];
        NSInteger lastResultIndex = [self getLastTestItemListFirstNum:[[CSVReader alloc]initWithData:[NSData dataWithContentsOfURL:[[NSURL alloc] initFileURLWithPath:path]] configuration:config] withTitleKey:[configDict objectForKey:@"TitleItemsName"]];
        
        CSVReader *reader = [[CSVReader alloc]initWithData:[NSData dataWithContentsOfURL:[[NSURL alloc] initFileURLWithPath:path]] configuration:config] ;
        NSError* error;
        
        NSArray *itemTargets = [configDict objectForKey:@"ItemColumTargets"];
        int TitleColumLength = [[configDict objectForKey:@"TitleColumLength"] intValue];
        
        NSInteger mainIndex = 1;
        
        
        while (!reader.isAtEnd) {
            
            NSArray *line = [reader readLineWithError:&error];
            // Skip to target row
            if(mainIndex < lastResultIndex){
                mainIndex++;
                continue;
            }
            // Main start method
            NSMutableDictionary *testItems = [dataDictionary objectForKey:@"TestItems"];
            
            NSString *UpperLimit        = [configDict objectForKey:@"UpperLimit"] ? [configDict objectForKey:@"UpperLimit"] : @"";
            NSString *LowerLimit        = [configDict objectForKey:@"LowerLimit"] ? [configDict objectForKey:@"LowerLimit"] : @"";
            NSString *MeasurementUnit   = [configDict objectForKey:@"MeasurementUnit"] ? [configDict objectForKey:@"MeasurementUnit"] : @"";
            
            if( mainIndex < lastResultIndex + TitleColumLength )
            {
                // For first test item config
                for(int i = 0 ; i < line.count ; i++)
                {
                    NSString *firstColumn = [NSString stringWithFormat:@"%@",line.firstObject];
                    NSString *subTargetTitle = [NSString stringWithFormat:@"%@",[line objectAtIndex:i]];
                    
                    if([subTargetTitle length]>0){
                        
                        if(mainIndex == lastResultIndex)
                        {
                            if([itemTargets containsObject:[NSNumber numberWithInteger:i]])
                                testItems = [dataDictionary objectForKey:@"TestItems"];
                            else
                                testItems = [dataDictionary objectForKey:@"CSVConfig"];
                            [testItems setObject:[@{@"columIndex":@(i),
                                                   @"Content":[@[]mutableCopy]
                                                   }mutableCopy]forKey:subTargetTitle];
                        }
                        else
                        {
                            if([firstColumn isEqualToString:UpperLimit] ||
                               [firstColumn isEqualToString:LowerLimit] ||
                               [firstColumn isEqualToString:MeasurementUnit])
                            {
                                for(int j = 0; j <line.count ; j++)
                                {
                                    testItems = [dataDictionary objectForKey:@"TestItems"];
                                    NSString *expectRowTitle = [line objectAtIndex:j];
                                    for (NSString *key in [[dataDictionary objectForKey:@"TestItems"] allKeys]) {

                                        NSInteger itemsNum = [[[testItems objectForKey:key] objectForKey:@"columIndex"] integerValue];

                                        if(j == itemsNum)
                                        {
                                            NSMutableDictionary *tempDict = [[dataDictionary objectForKey:@"TestItems"] objectForKey:key];
                                            [tempDict setObject:expectRowTitle forKey:firstColumn];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                // Save all colum data
                for(int i = 1 ; i < line.count ; i++){
                    NSString *subTitle = [line objectAtIndex:i];

                   if([itemTargets containsObject:[NSNumber numberWithInteger:i]])
                        testItems = [dataDictionary objectForKey:@"TestItems"];
                    else
                        testItems = [dataDictionary objectForKey:@"CSVConfig"];
                    for(NSString *key in  testItems.allKeys)
                    {
                        NSInteger index = [[[testItems objectForKey:key] objectForKey:@"columIndex"] integerValue];
                        if(i == index)
                        {
                            NSMutableArray *contentAry = [[testItems objectForKey:key] objectForKey:@"Content"];
                            [contentAry addObject:subTitle];
                            break;
                        }
                    }
                }
            }
            mainIndex++;
        }
    }
    return dataDictionary;
}



@end
