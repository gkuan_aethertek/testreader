//
//  main.m
//  TestReader
//
//  Created by Kuan George on 2020/4/22.
//  Copyright © 2020 Kuan George. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
    }
    return NSApplicationMain(argc, argv);
}
